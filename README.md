# test-semgrep-generic

The purpose of the repo is to test if the `paths` field in a semgrep rule can restrict the scan to specific folders and files, so that `generic` rules can be applied to only specific file type to avoid high overheads.

The basic idea is:
1. prepare the rules: two simple generic rules with the same pattern that captures the keyword findme, one uses

    paths:
      include:
        - "**/*.m"


to restrict the scan folders/files and the other does not.

2. prepare the source files for semgrep to scan: a bunch of simple test code in different languages, and put in different folders, in particular, an oc source file in a nested folder to test the recursive folder search ability

3. check the results: the two rules produce 20 findings (all of the findme in all folders) and 4 findings (only the findme in .m files) respectively, meaning paths works


## Getting started

Just ran the pipeline to see the result

alternatively, clone the repo to your local environment
```
1. git clone https://gitlab.com/hyan3/test-semgrep-generic.git
2. cd test-semgrep-generic
3. semgrep scan --config rule-path.yml 
4. semgrep scan --config rule.yml 
5. compare the results of 3 & 4
```
