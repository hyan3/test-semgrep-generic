#import <Foundation/Foundation.h>

// Method to find an element in an NSArray
NSInteger findme(NSArray *arr, NSInteger target) {
    NSUInteger index = [arr indexOfObject:@(target)];
    if (index != NSNotFound) {
        return index; // Return the index if found
    } else {
        return -1; // Return -1 if not found
    }
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Example usage
        NSArray *myArray = @[@1, @2, @3, @4, @5];
        NSInteger target = 3;
        NSInteger result = findme(myArray, target);
        if (result != -1) {
            NSLog(@"Element found at index: %ld", (long)result);
        } else {
            NSLog(@"Element not found");
        }
    }
    return 0;
}

