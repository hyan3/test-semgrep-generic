public class Main {
    public static void main(String[] args) {
        // Declare and initialize the variable findme
        int findme = 42;

        // Print the value of findme
        System.out.println("The value of findme is: " + findme);
    }
}
