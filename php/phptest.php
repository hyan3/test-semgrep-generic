<?php
// Function to find an element in an array
function findme($arr, $target) {
    $index = array_search($target, $arr);
    if ($index !== false) {
        return $index; // Return the index if found
    } else {
        return -1; // Return -1 if not found
    }
}

// Example usage
$myArray = array(1, 2, 3, 4, 5);
$target = 3;
$result = findme($myArray, $target);
if ($result !== -1) {
    echo "Element found at index: $result\n";
} else {
    echo "Element not found\n";
}
?>

